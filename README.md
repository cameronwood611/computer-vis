# Introduction

This repository is a brief overview of some of the topics and concepts learned in my Computer Vision course at the University of Alabama at Birmingham.

I credit all of my knowledge to my professor: Dr. John Johnstone (jkj@uab.edu).

All of this code was written by myself, Cameron Wood.

---

###### ETC.
author: Cameron Wood (@cameronwood611)
