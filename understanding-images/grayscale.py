# stl
import argparse

# pip
import cv2 as cv
import numpy as np


def read_img(fname: str) -> np.ndarray:
    im = cv.imread(fname)
    if not isinstance(im, np.ndarray):
        print(f"Unable to read image for the file path: {fname}")
        exit()
    return im


def img2gray(img: np.ndarray) -> np.ndarray:
    BGR_MAP = [0.0722, 0.7152, 0.2126]
    assert len(img.shape) == 3
    height = img.shape[0]
    width = img.shape[1]
    channels = img.shape[2]

    for i in range(height):
        for j in range(width):
            gs_val = 0
            for color in range(channels):
                pixel = img[i][j][color]
                gs_val += pixel * BGR_MAP[color]
            img[i][j] = [gs_val] * 3
    return img


def show_img(img: np.ndarray, title: str = "Image") -> None:
    cv.imshow(title, img)
    cv.waitKey()
    cv.destroyAllWindows()


def main(ARGV):
    im = read_img(ARGV.im_path)
    im_gray = img2gray(im)
    show_img(im_gray, "Grayscale")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Provide a color image to turn to grayscale")
    parser.add_argument(
        "--image",
        "-i",
        type=str,
        dest="im_path",
        default="lena_forsen.jpg",
        help="Provide file path to an image."
    )
    args = parser.parse_args()
    main(args)