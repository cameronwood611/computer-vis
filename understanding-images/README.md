# Image as a tensor

## Background

There are many ways to look at images: a function, sampling of pixels, projection of 3-space to 2-space, a tensor etc. This demonstration will take advantage of images being stored as a matrix (tensor).

For understanding images, color isn't that important for us. Interestingly, edge detection and the human eye notice luminance significalty more than color! Therefore, we deal with grayscale images.

- A grayscale image is an image whose pixels record luminance.
- Luminance of a pixel is essentially the perceived relative brightness (use of photoelectric effect, Spectral Sensitivity Functions, photodiode wells, etc.).

## Tensors

There could be a long descriptions of what tensors are but I will keep this brief.

A tensor is a higher-order generalization of a matrix. It could also be considered as the discretization of a continuous multivariate function. The order of a tensor is the number of its dimensions.

- Scalar is a 0-tensor.
- Vector is a 1-tensor.
- Matrix is a 2-tensor.
- Color image is a 3-tensor.
- Grayscale image is a 2-tensor.
- Video is a 4-tensor.

## Objectives

Build a color image from scratch and convert one to grayscale.

### Color Image

I will create a simple square image with blue, red, green and yellow quadrents.

This will demonstrate how images are interpreted in OpenCV.

Run with `pdm run python color_img.py`

### Grayscale

Since we use grayscale, I will demonstrate how to convert an image to grayscale. This can also be thought of as only rendering the images luminance.

Something to note is that OpenCV actually gives the matrices in row-major order (expected) and the color channels are in BGR order, not RGB.

Reading an image with OpenCV (at least in python) yields a numpy array and I will specify the data type of scalars to be uint8 (0-255), rather than a float32 (0-1).

Run with `pdm run python grayscale.py`
