
# pip
import cv2 as cv
import numpy as np



def build_color_image() -> np.ndarray:
    im = np.zeros((300, 300, 3), dtype=np.uint8)
    im[:150, :150, 0] = 255 # Only blue channel
    im[150:, :150, 1] = 255 # Only green channel
    im[:150, 150:, 2] = 255 # Only red channel

    # yellow
    im[150:, 150:, 2] = 255
    im[150:, 150:, 1] = 255

    return im


def main():
    im = build_color_image()
    cv.imshow("Colored image", im)
    cv.waitKey()
    cv.destroyAllWindows()


if __name__ == "__main__":
    main()