# STL

# PDM
import cv2 as cv
import numpy as np


def show_img(img: np.ndarray, title: str = "Image") -> None:
    cv.imshow(title, img)
    cv.waitKey()
    cv.destroyAllWindows()


def sobel_and_prewitt(img: np.ndarray):
    kx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]])
    ky = np.array([[-1,0,1],[-1,0,1],[-1,0,1]])
    
    imsx = cv.Sobel(img, cv.CV_8U, 1, 0, 1)
    show_img(imsx, "Sobel Vertical")
    impy = cv.filter2D(img, -1, ky)
    show_img(impy, "Prewitt Vertical")

    imsy = cv.Sobel(img, cv.CV_8U, 0, 1, 1)
    show_img(imsy, "Sobel Horizontal")
    impx = cv.filter2D(img, -1, kx)
    show_img(impx, "Prewitt Horizontal")
    show_img(impx + impy, "Prewitt")
    show_img(imsx + imsy, "Sobel")
    

def laplacian(img: np.ndarray):
    im = cv.Laplacian(img, cv.CV_8U, 3)
    show_img(im, "Laplacian")


def canny(img: np.ndarray):
    im = cv.Canny(img, 50, 100)
    show_img(im, "Canny Edge Detection")


def gaussian_blur(img: np.ndarray) -> np.ndarray:
    kernel = (3, 3)
    sigma = 0 #openCV calculates based off kernel
    gauss = cv.GaussianBlur(img, kernel, sigma, sigma)
    return gauss


def main():
    im = cv.imread("building.jpg")
    im = cv.cvtColor(im, cv.COLOR_BGR2GRAY)
    show_img(im, "Default")
    gauss = gaussian_blur(im)
    sobel_and_prewitt(gauss)
    laplacian(im)
    canny(im)


if __name__ == "__main__":
    main()